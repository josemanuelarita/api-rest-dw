<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $primaryKey = 'id_hotel';

    protected $table = 'hoteles';

    protected $fillable = [
        'nombre_hotel', 'ciudad_hotel', 'descripcion', 'habitaciones', 'imagenes', 
    ];

    public $timestamps = false;

}
