<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $fillable = ['check_in','check_out','total_huespedes','huespedes','total'];
    protected $timestamps = false;
}
