<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aerolinea extends Model
{

    protected $fillable = ['nombre', 'ciudad', ];

    public $timestamps = false;


}
