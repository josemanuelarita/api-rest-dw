<?php

namespace App\Http\Controllers;

use App\Aerolinea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AerolineaController extends Controller
{

    public function index()
    {
        return response()->json(Aerolinea::all());
    }

    public function store(Request $request)
    {

        $validation = Validator::make(
            $request->all(),
            [
                'nombre' => 'required',
                'ciudad' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json(['msj' => 'error'], 400);
        }else{
            $aerolinea = Aerolinea::create($request->all());
            return response()->json(
                ['msj' => 'agregado, id:' . $aerolinea->id], 
                200
            );
        }

        return response()->json($msj, $status);
    }

    public function show($id)
    {   
        $aerolinea = Aerolinea::find($id);
        
        if($aerolinea === null){
            return response()->json(
                ['msj' => 'no se encontro el recurso'],
                404
            );
        }else{
            return response()->json($aerolinea, 200);
        }

    }

    public function update(Request $request, $id)
    {
        $aerolinea = Aerolinea::find($id);

        if ($aerolinea === null) {
            return response()->json(
                ['msj' => 'no se encontro el recurso'],
                404
            );
        }else{
            $aerolinea->update($request->all());
            return response()->json(
                ['msj' => 'Actualizado'], 
                200
            );
        }
        
    }

    public function destroy($id)
    {

        $aerolinea = Aerolinea::find($id);

        if ($aerolinea === null) {
            return response()->json(
                ['msj' => 'no se encontro el recurso'],
                404
            );
        }else{
            $aerolinea->delete();
            return response()->json(
                ['msj' => 'Registro Eliminado'],
                200
            );
        }

    }
}
