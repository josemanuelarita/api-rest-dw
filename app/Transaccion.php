<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    protected $fillable = ['titulo','nombre','apellidos','email','telefono', 'metodo_pago','info_pago'];
    protected $timestamps = false;
}
