<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservacionhotelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservacionhoteles', function (Blueprint $table) {
            $table->increments('id_reservacionhotel');
            $table->string('fecha_checkin');
            $table->string('fecha_checkout');
            $table->integer('total_huespedes');
            $table->string('huespedes', 1000);
            $table->integer('total_habitaciones');
            $table->string('tipo_habitaciones', 100);
            $table->integer('hotel_id')->unsigned();
            $table->foreign('hotel_id')->references('id_hotel')->on('hoteles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
