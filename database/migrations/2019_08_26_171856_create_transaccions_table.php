<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('titulo', ['Sr.','Sra.','Srita.','Dr']);
            $table->string('nombre', 45);
            $table->string('apellidos', 45);
            $table->string('email', 128)->unique();
            $table->string('telefono', 45);
            $table->enum('metodo_pago', ['transferencia','tarjeta de credito','payPal']);
            $table->json('info_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaccions');
    }
}
